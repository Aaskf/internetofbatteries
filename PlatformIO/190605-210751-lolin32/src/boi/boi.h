/*
 * boi.h
 *
*/
// Don't import yourself twice
#ifndef __boi_h__
#define __boi_h__

#include <Arduino.h>
#include <Adafruit_INA219.h>

class boi // Battery of Internet class
{
  float total_current_used;

  public:
    // Sense current should return a float value
    float sense_current();

    // Check for current across VCC and ground pins, set switch
    bool enable_backpower();

    // Check current battery power level, set variable
    float sense_battery_level();
    // Respond if any buttons were pressed, set trigger
    enum Buttons
    {
      BTN_PWR,
      BTN_ACT,
      BTN_DEFLOCK,
      BTN_Count
    };

    // Check to see if we need to change our mode from button press, trigger on previous data
    int button_pressed(Buttons button);

    void set_button_pin(Buttons button, int pin);

    enum LEDs
    {
      LED_LEV20,
      LED_LEV40,
      LED_LEV60,
      LED_LEV80,
      LED_LEV100,
      LED_S_NODE,
      LED_S_BATT,
      LED_POUT_ON,
      LED_Count
    };

    void set_led_pin(LEDs led, int pin, int ledChannel);

    void set_led_pin_status(LEDs led, unsigned char Value);

    int read_ambient_sensor();

    enum CHGPinEnum
    {
      CHG_RDY,
      CHG_CHG,
      CHG_Count
    };

    void set_chg_pin(CHGPinEnum input, int pin);

    int get_chg_status();

    void set_charging_status();

    // Check for messages on web-server grafitti wall
    const char* get_grafitti();

    // Broadcast message to other devices via Wifi or BLE
    const char* broadcast_messages();

    const char* retrieve_messages_from_flash(char* message_type);

    bool store_message(char *FromMAC, char* message, unsigned int MessageLen);

  // private:
  private:
    int ButtonPins[BTN_Count];
    int LEDPins[LED_Count];
    int CHGPins[CHG_Count];
    int LEDChannels[LED_Count];
    int LEDFrequency[LED_Count];
};

#endif
