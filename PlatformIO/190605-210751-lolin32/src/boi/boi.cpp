/*
 *
 */

#include "boi.h"

// Initialize ledc variables
int freq = 5000;
int resolution = 8;

// Check for current across VCC and ground pins, set switch
bool boi::enable_backpower(){
    bool backpower = 0;

    // TODO: Check voltage on GAT pins and set backpower_enabled appropriately

    return backpower;
}

// Check current battery power level, set variable
float boi::sense_battery_level(){
    float battery_level = 9000.1;

    // TODO: Do stuff to get the battery's current capacity

    return battery_level;
}

// Check to see if we need to change our mode from button press, trigger on previous data
int boi::button_pressed(Buttons button){
    uint8_t ret;

    ret = digitalRead(this->ButtonPins[button]);

    return ret ? 0 : 1;
}

void boi::set_button_pin(Buttons button, int pin)
{
    this->ButtonPins[button] = pin;
    pinMode(pin, INPUT_PULLUP);
}

void boi::set_led_pin(LEDs led, int pin, int ledChannel)
{
    this->LEDPins[led] = pin;
    this->LEDChannels[led] = ledChannel;
    this->LEDFrequency[led] = freq;

    // pinMode(pin, OUTPUT);
    ledcSetup(this->LEDChannels[led], freq, resolution);
    ledcAttachPin(pin, this->LEDChannels[led]);
    ledcWrite(this->LEDChannels[led], 250);
}

void boi::set_led_pin_status(LEDs led, unsigned char Value)
{
    //TODO: ambient sensor requires reading in and being

    if(Value == 1){
        // this pattern will bring the LEDs to full brightness
        for (int dutyCycle = 128; dutyCycle <= 255; dutyCycle++) {
            ledcWrite(this->LEDChannels[led], dutyCycle);
            delay(2);
        }
    }
    if(Value == 0){
        // this pattern will bring the LEDs to no brightness
        for (int dutyCycle = 255; dutyCycle >= 128; dutyCycle--) {
            ledcWrite(this->LEDChannels[led], dutyCycle);
            delay(2);
        }
    }
}

int boi::read_ambient_sensor()
{
    int reading = 0;
    //TODO: return 0 to 1023
    //return analogRead(this->LEDPins[boi::LED_S_BATT]);
    return reading;
}

void boi::set_chg_pin(CHGPinEnum input, int pin)
{
    this->CHGPins[input] = pin;
}

int boi::get_chg_status()
{
    unsigned int read_chg_pin = 0;

    int status = 0;

    for(int i = 0; i < boi::CHG_Count; i++){
    // TODO: Read each of the CHG pins to see if we are charging or not
    }
    return status;
}

void boi::set_charging_status()
{
    //return 0 or 1
    //TODO: Retrieve plugged in status and light LED accordingly
    if(this->get_chg_status() == 0){
    //TODO:this->set_led_pin_status();
    }
}

// Check for messages on web-server grafitti wall
const char* boi::get_grafitti(){
    // TODO: Check to see if there is any new grafitti
    const char* grafitti = "It's not _supposed_ to work. -TwinkleTwinkie";

    return grafitti;
}

// Broadcast message to other devices via Wifi or BLE
const char* boi::broadcast_messages(){
    // TODO: Get messages stored in FLASH
    const char* messages = "42,DEEP_THOUGHT";

    // TODO: Broadcast Grafitti
    // TODO: Broadcast Master Message
    // TODO: Broadcast Score Updates
    // TODO: Broadcast Hash of scoreboard
    // TODO: Create ENUM to store broadcast options
    return messages;
}

const char* boi::retrieve_messages_from_flash(char* message_type){
    const char* messages = "IMMA CHARGIN MAH FLASH, TRUEROCKS";
    // get stored messages from FLASH
    // Case on message_type
        // Retrieve Grafitti
        // Retrieve Master Messages
        // Retrieve Score Updates
        // Retrieve Hash of scoreboard
    return messages;
}

bool boi::store_message(char *FromMAC, char* message, unsigned int MessageLen){
    // This function can be called to store received data in the FLASH if it hasn't already been written

    bool success = 0;

    // Add device to "seen devices" list if not already stored

    // Check if we've gotten this hash from that device before
    // if hashes don't match, broadcast entire scoreboard directly to other device to store

    // Split out data received in to it's type
    // Case statement to switch operation for different data types
    // Master Message
        // Perform master operation
        // Flash LED sequence for Master Message received
    // Score Update
        // Check to see if we have the right data for everyone we got data
        // Update broadcasting user
        // Read stored data for scoreboard
        // Loop through each item and update the FLASH if it has increased in value for that device ID
        // Flash LED sequence for "Scoreboard updated"
    // Grafitti
        // Flash LED sequence for "Grafitti Received"
    // Flash message received
        // Check to see if we already have the grafitti stored
    // Other

    // Perform function to store messages to FLASH only if new and in our format

    return success;
}
