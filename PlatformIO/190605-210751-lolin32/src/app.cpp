// Author: Aask
// Creation Date: 05/04/2019
//
// Preface: This is the main library for the DEF-CELL: WiFi Internet of Batteries for DEF CON 27
// This file contains everything needed to run the basic functions on the DEF CELL, as well as
// some other fun tools.

#include "app.h"
#include <Adafruit_INA219.h>

#define TEST

// Operating frequency of ESP32 set to 80MHz from default 240Hz in platformio.ini

// Initialize Global variables
float current_sensed; // This should be read out of Flash each boot, but not yet :)
//int delayMS = 100; // This will ultimately determine how sensitive the buttons are
int CurIdx = 0; // Track how many times we've looped

// Create global boi class
boi *Battery;

// initialize ina219
Adafruit_INA219 *ina219;

void setup(void) {

  // init system debug output
  Serial.begin(115200);

  // initialize boi
  Battery = new boi();

  ina219 = new Adafruit_INA219();
  ina219->begin();
  
  // Current Sensing Operation - Check to see if we have anything on the pins 

  Serial.println("Current Sensed:"+String(ina219->getCurrent_mA()));

  //TODO: Increment Flash storage location to increase amt of current current_sensed

  // Button assignment
  Battery->set_button_pin(boi::BTN_PWR, BTN_PWR_PIN);
  Battery->set_button_pin(boi::BTN_ACT, BTN_ACT_PIN);
  Battery->set_button_pin(boi::BTN_DEFLOCK, BTN_DEFLOCK_PIN);

  // LED assignment of pin and ledChannel
  Battery->set_led_pin(boi::LED_LEV20, LED_LEV20_PIN, 0);
  Battery->set_led_pin(boi::LED_LEV40, LED_LEV40_PIN, 1);
  Battery->set_led_pin(boi::LED_LEV60, LED_LEV60_PIN, 2);
  Battery->set_led_pin(boi::LED_LEV80, LED_LEV80_PIN, 3);
  Battery->set_led_pin(boi::LED_LEV100, LED_LEV100_PIN, 4);
  Battery->set_led_pin(boi::LED_S_NODE, LED_S_NODE_PIN, 5);
  Battery->set_led_pin(boi::LED_S_BATT, LED_S_BATT_PIN, 6);
  Battery->set_led_pin(boi::LED_POUT_ON, LED_POUT_ON_PIN, 7);

  // TODO: DNS setup
  // TODO: Default WiFi setup
  // TODO: MeshNetwork * MN = new MeshNetwork();
  // TODO: Default BlueTooth Setup
}

int CurLED = 0;
int PwrButton;

void loop() {
  // Check to see if a button was pressed or other event triggered
  if(Battery->button_pressed(boi::BTN_ACT)){
    // Test mode, turn on the LEDs sequentially!
    Serial.println("Running TEST BTN_ACT action!");
    Serial.println("Running sequential TEST for LEDs, lighting LED:!");
    Serial.println(CurLED);
    Battery->set_led_pin_status((boi::LEDs)CurLED, 1);

    // Increment the CurLED
    CurLED++;

    // If we hit the last entry in the LEDs enum shut all the LEDs off
    if(CurLED == boi::LED_Count)
    {
      for(int i = 0; i < boi::LED_Count; i++){
        Battery->set_led_pin_status((boi::LEDs)i, 0);
        }
      CurLED = 0;
    }
  }
  if(Battery->button_pressed(boi::BTN_PWR)){
    #ifdef TEST
      // Test mode! Turn on ALL of the LEDs
      Serial.println("Running TEST BTN_PWR button action!");
      Serial.println("Running group TEST of LEDs");
      for(int i = 0; i < boi::LED_Count; i++){
        Battery->set_led_pin_status((boi::LEDs)i, 1);
      }
      PwrButton ^= 1;
      
      Serial.println("Current Sensed:"+String(ina219->getCurrent_mA()));

    #else
      //code here for prod
      Serial.println("Running PROD BTN_PWR button action!");
      // TODO: Flush out reply to pressing the PWR button
      // Check for current across VCC and ground pins, set switch
      // If current on VCC, set initial power state to NO_BACKPOWER
      // If no current on VCC, set initial power state to BACKPOWER and BACKPOWER_LED to HIGH
    #endif
  }
  if(Battery->button_pressed(boi::BTN_DEFLOCK)){
    #ifdef TEST
      // Test mode! Turn on Backpower!
      Serial.println("Running TEST of DEFLOCK button");
      Serial.println("Running test of reading the current sensor!");

    #else
      //code here for prod
      Serial.println("Running PROD BTN_DEFLOCK button action!");
      // TODO: Flush out reply to pressing the PWR button
      // Check for current across VCC and ground pins, set switch
      // If current on VCC, set initial power state to NO_BACKPOWER
      // If no current on VCC, set initial power state to BACKPOWER and BACKPOWER_LED to HIGH
    #endif
  }

  // TODO: Check current power-draw on battery, set variable

  // TODO: Check for any nodes in the area, trigger on previous data

  // TODO: Check for updates based on data collected from nodes within reach, trigger on collected and/or previous data

  // TODO: Check for messages on web-server

  // TODO: Broadcast message to other devices via Wifi or BLE

  CurIdx++;
}
