/*
 * app.h
 *
*/
// Don't import yourself twice
#ifndef __app_h__
#define __app_h__
#include <Arduino.h>
#include "boi/boi.h" // Class library


// Comment the TEST line out if we are in prod mode
#define TEST // <- Uncomment for compiling TEST bin
#define BTN_PWR_PIN 12
#define BTN_ACT_PIN 19
#define BTN_DEFLOCK_PIN 13

#define LED_LEV20_PIN 25		// these should be in an array
#define LED_LEV40_PIN 26
#define LED_LEV60_PIN 27
#define LED_LEV80_PIN 14
#define LED_LEV100_PIN 16
#define LED_S_NODE_PIN 17
#define LED_S_BATT_PIN 5
#define LED_POUT_ON_PIN 18

#define LED_CHG_PIN 4 // Charging indicator

#define LED_AMB_PIN 2 // ? Not sure if this is the amb light sensor?

#endif