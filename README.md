# Internet of Batteries: DEF CON 27 - "They don't support that"

Welcome back to the Internet of Batteries: DEF CON 27 Edition! We've been told that the SAO standard "doesn't support" a battery back-powering the circuit on the VAC, which means I'm doing the most logical thing...Building a GAT (Greater Addon Technology) instead to support back-powering through it's pinout! While the GAT will be compatable with SAO 1.69bis, if plugged in to a DC27 badge it will most likely blow up the moon.

This project aims to create a simple (and mostly-safe) battery GAT which will back-power any electronic badge via VAC pins, supplying a 500mA @ 3.3V while keeping track of how much power is drawn via backpowering and forward powering other GATs. This GAT will run off of it's own power, and should be able to operate as a standalone unit. To view statistics users will be able to connect to the captive portal on the ESP32 to view local and shared battery discharge stats.

TODO: Create basic web form to host on ESP32  
TODO: Create Captive Portal for ESP32

Prototype IoB device testing:  
    [Back-powering DC DarkNet 7](images/DC_DarkNet_7.jpg)  
    [Back-powering MrRobot](images/MrRobot_Mockup.jpg)  
    [Back-powering MrRobot and Dickbutt SpaceHuhn](images/MrRobot_Powering_Dickbutt.jpg) 

## __Instructions: TEST__

The Internet of Batteries may be compiled in TEST mode by uncommenting the line "#define TEST" in the file [internetofbatteries.ino](./internetofbatteries.ino)

```c
#define TEST <- Uncomment for compiling TEST bin
```

### __BTN_ACT__

Each press of __BTN_ACT__ in _TEST_ mode will light an additional LED until they are all lit, and then turn them all off

### __BTN_PWR__

Each press of __BTN_PWR__ in _TEST_ mode will either turn on or off ALL of the LEDs

## __Instructions: PROD__

The Internet of Batteries may be compiled in PROD mode by commenting-out the line "#define TEST" in the file [internetofbatteries.ino](./internetofbatteries.ino)

```c
//#define TEST <- Comment for compiling PROD bin
//DFIU
//~Aask~
```

### __Action Button! (BTN_ACT)__

This button is for cycling between modes. Some of the modes will be persistent, others will time out and return to the default battery behaviorcycles through LED display modes

Mode (250ms press to cycle) | Timeout
-|-
Show current battery status | Persistent,brightness set by ambient light
Show local node count | Show for 10 seconds
Activate captive portal mode for viewing current stats | 30 seconds, or until user disconnects
LED Light-show | Persistent, show set in web interface

### __1.21 Jiggawatts! (BTN_PWR)__

This button is used for turning backpower on and off, though it's actually closer to 5-10 Watts

Mode (2000ms press to cycle) | Timeout
-|-
"passive-backpower" mode on <br/><br/>__This is NOT RECOMMENDED__| This will enable power to flow on the VCC pins for the male GAT<br/><br/>Begin flashing LED_POUT_ON<br/>Detect power on VCC for 3 seconds<br/>If no voltage on VCC pins, switch on backpower and enable LED_POUT_ON<br/>If voltage on VCC pins, fast flash LED_POUT_ON for 5 seconds and __do not enable backpower__
"passive-backpower" mode off <br/><br/>__This is HIGHLY RECOMMENDED__| <br/><br/>Begin flashing LED_POUT_ON for 3 seconds<br/>Disable backpower if on

## Da Rules

This is a contest of current! Meaning the more current you can pull from your badge and report back to the IoB the higher your score :battery:

__Some things to keep in mind__:

* Each badge, when in stock-configuration, has a known theoretical (Previously measured if possible) maximum current that may be drawn over the four days of DEF CON 27
* Score is the measure of mAh drawn from the onboard current sensor
* Badges will broadcast their total mAh consumed, as well as other badge's scores which are stored in memory (BROADCAST_INTERVAL)
* Badges will automatically trigger and decode received messages from other badges (if able/not broadcasting), and store that device's score locally/update the rest of the scoreboard for rebroadcast

__How to win__:

* Scores must be received on Aask's badge/primary node by NOON on August 11th, 2019 (Or 1.5 hours before contest close)
* The winner (of verified current draw) will be unofficially declared __"DEF CON's Elon"__ in honor of Mr. Musk's crazy advances in energy storage technologies and push to make a sustainable future for us all to explore our amazing Universe
* If BYOD is implemented, any winning devices must be shown to be functioning in person by Aask, true, or Lightning, to have their score validated

## Required Features

* Auto-detect power on VAC, only provide back-power if no voltage
* I am thinking like: press and hold backpower button (2) to turn on backpower, automatically turn off if no current flow detected. detect voltage first etc
* Code function for handling each function: "Backpowering"
* Sticker-sheet for instructions on the back
* wifi should be on all the time to broadcast data to people scanning on phone
* just have to be aware of when broadcasting, knowing when there is association, then sleeping during dtim period if no active xfer
* Private-Captive Portal mode for viewing personal stats
* Graffiti-wall for other badge owners/authorized users to leave a message

## Boot Order

This is very important and will help to minimize blowing up like a Note in the sky

1. Detect power on rails

* If power, set flag for NO backpower, even if switch is on
* If no power, set flag for YES backpower, even if switch is off

1. Current Sensing Operation - Determine default configuration
2. If current on VCC, set initial power state to NO_BACKPOWER
3. If no current on VCC, set initial power state to BACKPOWER and BACKPOWER_LED to HIGH
4. Button assignment  
```BTN_PWR, BTN_ACT```
5. LED assignment  
```LED_LEV20, LED_LEV40, LED_LEV60, LED_LEV80, LED_LEV100, LED_S_NODE, LED_S_BATT, LED_POUT_ON```
6. DNS setup
7. Default WiFi setup
8. Default BlueTooth Setup
9. React to user input

## BOM Actual

Asterisks mark parts true has on hand. Slashes mark partial supply. Prices are approximate.

GPIO Count roughly: 17

- `.$?.??` PCB (dimensions `X x Y`)  
- `*$3.00` ESP32-WROOM-32
- `.$1.00` INA219 (https://www.aliexpress.com/item/100PCS-LOT-INA219AIDCNR-SOT23-8/32921376811.html)
- `.$?.??` shunt resistor (~0.2ohm, 1A max, 500mA expected, 0.1mA reporting resolution)
- `.$?.??` current sensor [todo] 
- `.$3.60` Battery (idea: https://www.aliexpress.com/item/10-pcs-1500mAh-3-7V-Li-Po/32804526236.html) (cheaper / smaller: https://www.aliexpress.com/item/10-pcs-3-7V-1200mAh-103040-LiPo/32516208893.html)
- `.$0.50` JST board connector, JST header for battery
- `*$0.20` TP4056+DW01+FS8205 charger and protection circuit
- `*$0.75` 3v3 ~700mA peak buck voltage regulator
- `/$0.40` CH340C for USB-serial comms, debugging
- `*$0.10` microUSB for charging
- `*$1.00` misc passives (likely hi
- `*$0.10` LEDs
  - esp: 2x power supplying, receiving
  - esp: mesh units found (1, 2, 3, 4+) - combined with next line
  - esp: 5x battery life left (20%, 40%, 60%, 80%, 100%)
  - esp: e or 2x showing batt life statusfuckaround?
  - 4056: 2x charge active, charge done/ready
- `*$0.15` button(s)
   - message send sort of button? or ok / action button to confirm, say, setting of information via the web interface (to help prevent attacks)?
   - enable backpower function
   - esp: io0
   - esp: !en
 - `.$?.??` NRE, prototyping, assembly, ...

total cost roughly ~$15-20/ea

## Board Layout

Front:

```txt
      _____
 ____|ESP32|____ ---
|    |     | ON | |   * Toggles on either side of the ESP32
|    |_____| OFF| |   * LEDS for power-on, voltage selected, 
|   |GAT Mnt|   | |		new battery data received,
|LEDs=======LEDs| |     and misc (5x total if pinout supports everything)
|               | 6.5cm?  * GAT mount for standalone use
|    Battery    | |   * 3d printed mount for the battery
|    Goes       | |     for easy drop in replacement
|    Here       | |
|_______________| |
                 ---
|-----3.5cm?----|

Back: 
      _____
 ____|_____|____
|3.3v|Chgr |    |     * Charger on the back side of the ESP32
|Reg |_____|    |     * Current regulators next to charger
|    |_GAT_|    |     * GAT is square in the center of gravity of
|     _____     |     the device for attaching to badges
|    | Cur |    |
|    |Sense|    |     * Current sensor can be moved around
|    |_____|    |
|  Velcro Tape  |     * Not sure how I want to secure this to devices, maybe 
|_______________|

```


### Requirements

- badge is always powered by its own battery if battery is on. if not backpowering, deassert PWR_EN.

- badge is always forward-powered through GAT whether battery is on or not, if power is present. There will be an initial voltage drop so PWR_EN should be turned on if there is no voltage on VBAT to turn on the power transistor and allow full voltage operation.

- to check VBAT voltage: make sure pullup on VBAT_DIV is enabled, set ADC Vref to main voltage rail. if measured full ADC range, then BATTERY IS DISCONNECTED as battery GND will be floating. Otherwise will have divided battery level, see those levels in the schematic. Should be comparing against bandpass reference.

- to check VGAT voltage: this is always connected and may show a similar value to VBAT_DIV when we are backpowering. so in this case, measure CURRENT FLOW to ensure we are not receiving power in. basically will be voltage of battery (if enabled), or VGAT (if batt disabled).

- badge only monitors BATTERY CURRENT TO / FROM GAT. it does not monitor its own power draw.

- badge MUST try to turn off any connection to GAT power (though GAT will flow in) when battery gets flipped on, unless "emergency UPS" mode is enabled (possibly hard to find / in the web interface).

- badge can BACKPOWER when following conditions are true: 1) badge has battery power and has not shut down for power safety, 2) there is enough charge to initiate this mode as indicated by VBAT_DIV, 3) GAT is currently showing no voltage present.

- after BACKPOWER power is on, shut off if current is flowing back INTO badge. shut off if current exceeds threshold (should be configurable in web interface, would recommend 250mA default, up to 400mA configurable, to not blow up parts)

- backpower is off, PWR_EN is set low. if enabled, PWR_EN is set high.

- there should be a manual in the web interface...